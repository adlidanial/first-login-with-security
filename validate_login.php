<?php
    $email = "";
    $pwd = "";
    $pwdHash = "";
        
    $email = $_POST["email"];
    $pwd = trim($_POST["password"]);
    
    include_once 'connect.php';
   
    $sql = "SELECT * FROM user WHERE EMAIL = ?";
    $stmt = $conn->prepare($sql);
    $stmt->execute([$email]);

    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $pwdHash = $row["PASSWORD"];
    }

    $isChecking = password_verify($pwd, $pwdHash);
    if($isChecking)
    {     
        echo "1";
    }
    else
    {
        echo "0";
    }
        
    $conn = null;
?>