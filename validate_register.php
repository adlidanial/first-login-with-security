<?php
    $email = "";
    $pwd = "";
    $pwdHash = "";
        
    $email = $_POST["email"];
    $pwd = trim($_POST["password"]);
        
    $pwdHash = password_hash($pwd, PASSWORD_DEFAULT);

    include_once 'connect.php';

    $sql = "SELECT * FROM user WHERE EMAIL = ?";
    $stmt = $conn->prepare($sql);
    $stmt->execute([$email]);
    $posts = $stmt->rowCount();

    if($posts > 0)
    {     
        echo "0";
    }
    else
    {
        $sql = "INSERT INTO user(EMAIL, PASSWORD) VALUES (:email, :password)";
        $stmt = $conn->prepare($sql);
        $stmt->execute([
                "email" => $email,
                "password" => $pwdHash
        ]);
              
        echo "1";
    }

    $conn = null;
?>