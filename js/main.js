$(document).ready(function() {
	$('#email').focus(function(){
		$('.box-failed').css("display", "none");
		$('.msg-alert').text('');
	});

	$('#password').focus(function(){
		$('.box-failed').css("display", "none");
		$('.msg-alert').text('');
	});

	$('#re-password').focus(function(){
		$('.box-failed').css("display", "none");
		$('.msg-alert').text('');
	});


	$('#btnRegSubmit').click(function(e) {
		e.preventDefault();
		var email = $('#email').val();
		var pwd = $('#password').val();
		var rePwd = $('#re-password').val();
		var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;			
		
		if(!emailReg.test(email))
		{
			$('#email-alert').text('*Please enter correctly email!');
		}
			
		if(pwd.length < 8)
		{
			$('#pwd-alert').text('*Password must be minimum length of 8');
		}

		if(rePwd.length < 8)
		{
			$('#re-pwd-alert').text('*Password must be minimum length of 8');
		}

		if(pwd != rePwd)
		{
			$('#re-pwd-alert').text('*Password must be same on above');
		}

		if(emailReg.test(email) && pwd != "" && rePwd != "" && pwd==rePwd)
		{
			$.ajax({
				type: 'POST',
				url:  'validate_register.php',
				data: {email:email, password:pwd},
				success: function(data){
					if(data == "0")
					{
						$('.box-failed').css("display", "block");
					}
					else if(data == "1")
					{
						$('.box-failed').css("display", "none");
						alert('Successful registered!');
						location.href = "index.html";
					}
				}
			});
		}
	});

	$('#btnSubmit').click(function(e) {
		e.preventDefault();
		var email = $('#email').val();
		var pwd = $('#password').val();

		if(email == ""){
			$('#email-alert').text('*Please fill in the blank!');
		}

		if(pwd == ""){
			$('#pwd-alert').text('*Please fill in the blank!');
		}

		if(email != "" && pwd != "")
		{
			$.ajax({
				type: 'POST',
				url:  'validate_login.php',
				data: {email:email, password:pwd},
				success: function(data){
					console.log(data);
					if(data == "0")
					{
						$('.box-failed').css("display", "block");
					}
					else if(data == "1")
					{
						$('.box-failed').css("display", "none");
						location.href = "success.html";
					}
				}
			});
		}
	});
});
